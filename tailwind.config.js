module.exports = {
  purge: {
    content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    safelist: ['bg-red-500', 'bg-green-500', 'bg-blue-500', 'bg-yellow-500'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Noto Sans JP', 'Sans-serif'],
      },
      backgroundImage: {
         'login-texture': "url('/src/assets/earth1.png')",
        },
    },
  },
  variants: {
    extend: {

    },
  },
  plugins: [require('@tailwindcss/forms')],
}
