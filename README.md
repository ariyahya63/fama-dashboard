# Tailmin Admin Dashboard Template - [Live Demo](https://tailmin.vercel.app/)

Admin dashboard built with Vue.js and Tailwind CSS

![Tailmin Admin Dashboard Template!](https://user-images.githubusercontent.com/2015833/130907375-07bfddc9-5886-4522-b32a-5e1757eeb6b4.png)

## Usage

1. Clone this repository
2. Run `npm install` to install dependencies
3. Run `npm run dev` to start development server
4. Go to http://localhost:3000

## Docker Local Test

Menjalankan FAMA-DASHBOARD ADMIN pada Docker Local:

1. git clone [project:fama-dashboard](http://git.xyz/riko/fama-dashboard.git)
2. Jalankan 'docker-compose up -d --build'
3. Tunggu sampai build selesai
4. Akses website melalui url [http://localhost:3000](http://localhost:3000)
