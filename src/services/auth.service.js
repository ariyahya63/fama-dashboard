import axios from 'axios';

const API_URL = 'http://10.10.50.104:8000/api/auth/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login', {
        email: user.email,
        password: user.password
      })
      .then(response => {
        if (response.data.success) {
          // console.log(response.data.success)
          localStorage.setItem('user', JSON.stringify(response.data.data));
          localStorage.setItem('token',JSON.stringify(response.data.access_token));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();
