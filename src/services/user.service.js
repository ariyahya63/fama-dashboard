import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8000/api/web/';

class UserService {
  getUser(id) {
    return axios.get(API_URL + 'profile/' + id, { headers: authHeader() });
  }

  updateProfile(id, data) {
    return axios.post(API_URL + 'profile/' + id, data, { headers: authHeader() });
  }

  changePassword(data) {
    return axios.post(API_URL + 'change-password', data,  { headers: authHeader() })
  }
}

export default new UserService();
