import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: () => import('@/views/Dashboard.vue'),
    children: [
      {
        path: '',
        component: () => import('@/views/dashboard/Home.vue'),
      },
      {
        path: 'users',
        name: 'user-list',
        component: () => import('@/views/dashboard/users/UserList.vue'),
      },
      {
        path: 'reports',
        name: 'report-list',
        component: () => import('@/views/dashboard/report/ReportList.vue'),
      },
      {
        path: 'visual-surveillance',
        name: 'visual-surveillance',
        component: () => import('@/views/dashboard/opencctv/VisualSurveillance.vue'),
      },
      {
        path: 'geo/fama-report',
        name: 'geo-fama',
        component: () => import('@/views/dashboard/report/FamaGeoReport.vue'),
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import('@/views/dashboard/Profile.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior() {
    return { top: 0 }
  },
  routes,
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})

export default router
