import { createApp } from 'vue'
import App from './App.vue'
import './assets/css/tailwind.css'
import router from './router'
import store from './store'
// import vuelidate from 'vuelidate'

const app = createApp(App)
app.use(store)
app.use(router)
// app.use(vuelidate)

app.mount('#app')
